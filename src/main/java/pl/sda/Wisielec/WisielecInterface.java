package pl.sda.Wisielec;

import java.util.Scanner;

public class WisielecInterface {
	
	public Integer WyborTrybu() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("1. Tryb gracza");
		System.out.println("2. Tryb administratora");
		System.out.println("3. Koniec");
		Integer opcja = scanner.nextInt();

		return opcja;
	}

	public String Autoryzacja() {
		WisielecSzyfr wisielecSzyfr = new WisielecSzyfCezara();
		System.out.println("Podaj hasło: (Podpowiedź imię autora projektu) ");
		Scanner scanner = new Scanner(System.in);
		String haslo = scanner.nextLine();
		String hasloZakodowane = wisielecSzyfr.szyfruj(haslo);
		return hasloZakodowane;
	}
	
	public Integer menu() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("1. Start");
		System.out.println("2. Koniec");

		Integer opcja = scanner.nextInt();
		return opcja;

	}
	
	public Integer menuAdmina() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("1. Dodaj pytanie");
		System.out.println("2. Koniec");

		Integer opcja = scanner.nextInt();
		return opcja;

	}
	
	public String playerName() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Podaj imię gracza:");
		String imie=scanner.nextLine();
		return imie;
		
	}
	
	
}
