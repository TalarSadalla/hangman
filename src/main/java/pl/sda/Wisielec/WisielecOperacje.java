package pl.sda.Wisielec;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;


public interface WisielecOperacje {	


	List<String> pytaniaZPlikuJakoLista(File file) throws FileNotFoundException;

	//void zapiszWynikDoPliku(File plikZWynikami, Wynik wynik) throws IOException;

	//List<Wynik> wynikiZPlikuJakoLista(File plikZWynikami) throws FileNotFoundException;

	String pobieranieNowegoHasla();

	void zapiszPytanieDoPliku(File plikZWynikami, String pytanie) throws IOException;
}
