package pl.sda.Wisielec;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class WisielecOperacjeZPliku implements WisielecOperacje {
	
	private WisielecSzyfr wisielecSzyfr = new WisielecSzyfCezara();	
	
	public List<String> pytaniaZPlikuJakoLista(File file) throws FileNotFoundException {

		ArrayList<String> listaPytan = new ArrayList<>();
		Scanner scanner = new Scanner(file);
		while (scanner.hasNext()) {	
			listaPytan.add(wisielecSzyfr.rozszyfruj(scanner.nextLine()));		
		}
		return listaPytan;

	}
	
	public String pobieranieNowegoHasla() {
		Scanner scanner = new Scanner(System.in);
		String pytanie=null;
		List<String> listaOdpowiedzi = new ArrayList<>();
		System.out.println("Podaj nowe haslo: ");
		pytanie=scanner.nextLine();
		String zakodowanePytanie=wisielecSzyfr.szyfruj(pytanie);
		return zakodowanePytanie;
	}
	

	@Override	
	public void zapiszPytanieDoPliku(File plikZWynikami, String pytanie) throws IOException {
		try (FileWriter fw = new FileWriter(plikZWynikami, true);
				BufferedWriter bw = new BufferedWriter(fw);
				PrintWriter out = new PrintWriter(bw)) {
			String wynikJakoString = pytanie.toString() + "\r\n";
			out.print(wynikJakoString);
		}
	}
	
}
