package pl.sda.Wisielec;

public interface WisielecSzyfr {
	String szyfruj(String wiadomosc);

	String rozszyfruj(String zakodowanaWiadomosc);
}
